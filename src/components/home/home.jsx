import React from 'react';
import Imagens from '../imagens/pig-standing.svg'
import Imagens2 from '../imagens/how-to-1.svg'
import Imagens3 from '../imagens/how-to-2.svg'
import Imagens4 from '../imagens/how-to-3.svg'
import Imagens5 from '../imagens/goals.svg'
import './home.css'

function Home() {
    return(
        <>
            <section id="sobre">
                <div>
                    <h1>Ping Bank</h1>
                    <p>Lorem ipsum dolor sit amet<br/> consectetur adipisicing elit.<br/> Nesciunt quas laudantium<br/> eos possimus odit labore<br/>pariatur optio.<br/> Amet consequatur nihil,<br/>tempore maiores placeat<br/> commodi ratione pariatur<br/>quas iste debitis atque.</p>
                    <button>NOVA META</button>
                </div>
                <img src={Imagens} alt={"pig-standing.svg"}/>
            </section>
            <br></br>
            <section id="body">
            <div>
                <h2>Como usar</h2>
                <img src={Imagens2} alt={"how-to-1.svg"}/>
                <h4> DEFINA UMA META</h4>
                <p>Phalenfrhyfn sjhf7ryrhg<br/> ierh73hefngvn hejrirh</p>
            </div>
            <div>
                <img src={Imagens2} alt={"how-to-2.svg"}/>
                <h4>   ACOMPANHE O PROGRESSO</h4>
                <p>Phalenfrhyfn sjhf7ryrhg<br/> ierh73hefngvn hejrirh</p>
            </div>
            <div>
                <img src={Imagens4} alt={"how-to-3.svg"}/>
                <h4> ALCANCE SEU OBJETIVO</h4>
                <p>Phalenfrhyfn sjhf7ryrhg<br/> ierh73hefngvn hejrirh</p>
            </div>
            </section>
            <section id="footer">
                <div>
                    <h4>Já tem uma meta?</h4>
                    <p>Phalenfrhyfn sjhf7ryrhg<br/> ierh73hefngvn hejrirh</p>
                    <button>VER METAS</button>
                </div>
                <img src={Imagens5} alt={"goals.svg"}/>
            </section>
            <div class="pe">
                <h3>Não tem Comece agora mesmo</h3>
            </div>

        </>      
    )
}

export default Home
